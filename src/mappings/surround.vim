" Vem: surround

" surround visual selection
vmap <Plug>vem_surround- <Plug>VSurround

" delete surrounding markers
nmap <Plug>vem_delete_surround- <Plug>Dsurround

" change surrounding markers
nmap <Plug>vem_change_{- <Plug>Csurround{
nmap <Plug>vem_change_}- <Plug>Csurround}
nmap <Plug>vem_change_[- <Plug>Csurround[
nmap <Plug>vem_change_]- <Plug>Csurround]
nmap <Plug>vem_change_(- <Plug>Csurround(
nmap <Plug>vem_change_)- <Plug>Csurround)
nmap <Plug>vem_change_<- <Plug>Csurround<
nmap <Plug>vem_change_>- <Plug>Csurround>
nmap <Plug>vem_change_t- <Plug>Csurroundt
nmap <Plug>vem_change_`- <Plug>Csurround`
nmap <Plug>vem_change_'- <Plug>Csurround'
nmap <Plug>vem_change_"- <Plug>Csurround"
nmap <Plug>vem_change_surround- <Plug>Csurround

