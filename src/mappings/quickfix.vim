" Vem: quickfix

" Browse quickfix results
nnoremap <silent> <Plug>vem_quickfix_prev_result- :cprev<CR>
nnoremap <silent> <Plug>vem_quickfix_next_result- :cnext<CR>

" Browse location window results
nnoremap <Plug>vem_location_prev_result- :lprev<CR>
nnoremap <Plug>vem_location_next_result- :lnext<CR>

